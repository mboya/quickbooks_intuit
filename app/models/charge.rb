# == Schema Information
#
# Table name: charges
#
#  id            :uuid             not null, primary key
#  txn_charge_id :string           not null
#  amount        :integer          default(0), not null
#  response      :jsonb            not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  synced        :boolean          default(FALSE)
#
class Charge < ApplicationRecord
  after_commit :call_quickbook

  private

  def fetch_customer_ref
    FetchCreateCustomer.new(response['data']['attributes']['sender_no']).call
  end

  def call_quickbook
    QuickbookPayment.new(amount, fetch_customer_ref).call if amount.positive?
  end
end
