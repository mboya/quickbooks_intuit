# frozen_string_literal: true

# Charge
class ChargesController < JSONAPI::ResourceController
  def callback
    QuikkCallback.new(params).call
    render json: :ok
  end
end
