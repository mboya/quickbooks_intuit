# frozen_string_literal: true

# quickbooks
class QuickbookPayment < ApplicationService
  attr_reader :amount, :customer_ref_id

  # rubocop:disable Lint/MissingSuper
  def initialize(amount, customer_ref_id)
    @amount = amount
    @customer_ref_id = customer_ref_id
  end

  # rubocop:enable Lint/MissingSuper

  def call
    post_payment
  end

  private

  def post_payment
    Rails.logger.info uri
    http_client.post(uri, options)
  end

  def uri
    "https://#{ENV['INTUIT_URL']}/v3/company/#{ENV['INTUIT_COMPANY_ID']}/payment?minorversion=#{ENV['INTUIT_MINOR_VERSION']}"
  end

  def auth_headers
    {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': "Bearer #{ENV['INTUIT_TOKEN']}"
    }
  end

  def req_body
    {
      TotalAmt: amount,
      CustomerRef: {
        value: customer_ref_id
      }
    }.to_json
  end

  def options
    {
      body: req_body,
      headers: auth_headers
    }
  end

  def http_client
    HTTParty
  end
end
