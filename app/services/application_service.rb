# frozen_string_literal: true

# application
class ApplicationService
  class << self
    def call(*args)
      new(args).call
    end
  end
end
