# frozen_string_literal: true

# Fetch Create
class FetchCreateCustomer < ApplicationService
  attr_reader :phone

  # rubocop:disable Lint/MissingSuper
  def initialize(phone)
    @phone = phone
  end

  # rubocop:enable Lint/MissingSuper

  def call
    intuit_customer
  end

  private

  def url
    "https://#{ENV['INTUIT_URL']}/v3/company/#{ENV['INTUIT_COMPANY_ID']}/query?query=#{query}"
  end

  def create_url
    "https://#{ENV['INTUIT_URL']}/v3/company/#{ENV['INTUIT_COMPANY_ID']}/customer?minorversion=#{ENV['INTUIT_MINOR_VERSION']}"
  end

  def intuit_customer
    response = http_client.get(url, options)
    response['QueryResponse'].try(:empty?) ? create_customer : response['QueryResponse']['Customer'][0]['Id']
  end

  def create_customer
    res = http_client.post(create_url, create_options)
    res['Customer']['Id']
  end

  def auth_headers
    {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': "Bearer #{ENV['INTUIT_TOKEN']}"
    }
  end

  def options
    {
      headers: auth_headers
    }
  end

  def create_options
    {
      headers: auth_headers,
      body: req_body
    }
  end

  def req_body
    {
      DisplayName: phone,
      PrimaryPhone: {
        FreeFormNumber: phone
      }
    }.to_json
  end

  def query
    "select Id from Customer Where DisplayName = '#{phone}'"
  end

  def http_client
    HTTParty
  end

end
