# frozen_string_literal: true

# callback
class QuikkCallback < ApplicationService
  attr_reader :args, :amount_in, :charge_id

  # rubocop:disable Lint/MissingSuper
  def initialize(args)
    @args = args
    @amount_in = args['data']['attributes']['amount'].present? ? args['data']['attributes']['amount'] : 0
    @charge_id = args['data']['attributes']['txn_charge_id']
  end

  # rubocop:enable Lint/MissingSuper

  def call
    save!
  end

  private

  def charge
    Charge
  end

  def save!
    charge.create({ txn_charge_id: charge_id, amount: amount_in, response: args })
  end

end
