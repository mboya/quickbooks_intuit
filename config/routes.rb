Rails.application.routes.draw do
  get 'up' => 'rails/health#show', as: :rails_health_check
  get '/', to: ->(_) { [200, { 'Content-Type' => 'application/json' }, [{ status: 'ok' }.to_json]] }

  # jsonapi_resources :charges
  post 'callback' => 'charges#callback', as: :charge_callback
end
