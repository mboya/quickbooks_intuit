# frozen_string_literal: true

class AddSyncedToCharges < ActiveRecord::Migration[7.0]
  def change
    add_column :charges, :synced, :boolean, default: false
  end
end
