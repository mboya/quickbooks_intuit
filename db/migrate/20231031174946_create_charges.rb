class CreateCharges < ActiveRecord::Migration[7.0]
  def change
    create_table :charges, id: :uuid do |t|
      t.string :txn_charge_id, null: false
      t.integer :amount, null: false, default: 0
      t.jsonb :response, null: false

      t.timestamps
    end

    add_index :charges, :txn_charge_id
    add_index :charges, :response

    add_check_constraint :charges, 'amount >= 0', name: 'amount_check'
  end
end
